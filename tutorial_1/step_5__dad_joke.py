import json
from botocore.vendored import requests


def lambda_handler(event, context):
    joke = get_joke()

    return {
        'statusCode': 200,
        'body': json.dumps({
            'text': joke,
            'response_type': 'in_channel'
        })
    }


def get_joke():
    url = 'https://icanhazdadjoke.com/'
    headers = {
        'Accept': 'text/plain'
    }
    result = requests.get(url, headers=headers)

    if result.status_code == 200:
        return result.text

    return None
