Serverless Slack Bot
====================

The files in this repository are from my tutorials on creating a Serverless Slack Bot.

You can read the first tutorial on the [MessageMedia Developer Website](https://developers.messagemedia.com/writing-a-serverless-slack-bot/).